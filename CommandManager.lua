if not rawget(_G, "CommandManager") then
	rawset(_G, "CommandManager", {})
	-- CommandManager settings.
	CommandManager.command_prefix = "/"
	CommandManager.retMessage = true

	if #tweak_data.chat_colors == 4 then
		table.insert(tweak_data.chat_colors, Color(1, 1, 0))
	end

	-- CommandManager Globals
	function trim(s) -- Remove Spaces
		return s:match("^%s*(.-)%s*$")
	end

	-- in-game check
	function inGame()
	    if not game_state_machine then return false end
	    return string.find(game_state_machine:current_state_name(), "game")
	end

	-- Show a message in HUD
	function ShowHint(message,duration)
		if managers and managers.hud then
			managers.hud:show_hint({text = message,time = duration})
		end
	end

	-- Send A Message to a Player
	function CommandManager:Send_Message(peer_id, message)
		if not message or message == "" then 
			return
		end

		if not string.find(message, ":") then
			message = "-: "..message
		end

		if peer_id == managers.network:session():local_peer():id() then
			managers.menu:relay_chat_message(message, 5)
		else
			local peer = managers.network:session():peer(peer_id)
			if peer then
				managers.network:session():send_to_peer(peer, "sync_chat_message", message)
			end
		end
	end

	-- Command Handler
	function CommandManager:Command(message, peer_id)
		if not inGame() and not (managers.network:session() and managers.network:session():peer(peer_id)) then return end
		local peername = managers.network:session():peer(peer_id):name()
		local peeruserid = managers.network:session():peer(peer_id):user_id()
		local lpeer_id = managers.network:session():local_peer():id()
		if not peername then return end

		if string.sub(message,1,1) ~= CommandManager.command_prefix then
			return
		end

		if string.len(message) == 1 then
			return
		end

		-- Carga las unidades
		local unit = managers.network:game():unit_from_peer_id(peer_id)
		local lunit = managers.player:player_unit()

		-- CommandManager Stuff
		local lowermsg = message:lower()
		local ret -- return string de comandos

		if string.sub(lowermsg,2,5) == "test" then
			LightLog("true")
			ret = "true"
		end

		-- Comando lowercmd
		for command in string.gmatch(message, '[^' .. self.command_prefix .. ']+') do
			command = trim(command) -- Quita los espacios de más
			
			-------------------------------------------------------------
			-- Remplaza con su palabra clave.
			-------------------------------------------------------------
			-- command = CommandManagerReplaceKeywords(command) -- por ejemplo: %id% devuelve el peer_id
			-- command = command:gsub("&", '.') -- Remplaza '&' por '.' para aludir el parser y poder usar el delimiter como parametro de un comando.
			-------------------------------------------------------------
			local lowercmd = command:lower() -- el comando en minuscula

			if string.sub(lowercmd,1,4) == "hint" then
				local msg = trim(command:sub(6))
				ShowHint(tostring(msg),10)
			end

			if self.retMessage and ret then
				self:Send_Message(peer_id, command..": "..ret)
				ret = nil
			end
		end
	end
end

if RequiredScript == "lib/managers/hudmanager" then
	function HUDManager:_cb_chat()
		local hud = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN)
		local chat_text = hud.panel:child("chat_input"):child("text"):text()

		if managers.network:session() and chat_text and tostring(chat_text) ~= "" then
			if string.sub(tostring(chat_text),1,1) == CommandManager.command_prefix then
				CommandManager:Command(tostring(chat_text), managers.network:session():local_peer():id())
			else
				local say = string.format("%s: %s", managers.network:session():local_peer():name(), tostring(chat_text))
				if managers.network:session() then
					self:_say(say, managers.network:session():local_peer():id())
					managers.network:session():send_to_peers("sync_chat_message", say)
				end
			end
		end

		self._chatbox_typing = false
		hud.panel:child("chat_input"):child("text"):set_text("")
		hud.panel:child("chat_input"):child("text"):set_selection(0, 0)
		setup:add_end_frame_clbk(function()
			self:set_chat_focus(false)
		end)
	end
end

if RequiredScript == "lib/managers/menu/menunodegui" then
	function MenuNodeGui:_cb_chat(row_item)
		local chat_text = row_item.chat_input:child("text"):text()
		if chat_text and tostring(chat_text) ~= "" then
			-------------------------------
			if string.sub(tostring(chat_text),1,1) == CommandManager.command_prefix then
				CommandManager:Command(tostring(chat_text), managers.network:session():local_peer():id())
			else
				local say = string.format("%s: %s", managers.network:session():local_peer():name(), tostring(chat_text))
				-------------------------------
				if managers.network:session() then -- comprobacion agregada		
					self:_say(say, row_item, managers.network:session():local_peer():id())
					managers.network:session():send_to_peers("sync_chat_message", say)
				end
			end
		end

		self._chatbox_typing = false
		row_item.chat_input:child("text"):set_text("")
		row_item.chat_input:child("text"):set_selection(0, 0)
	 end
end